﻿* Encoding: UTF-8.
SET ERRORS OFF.

COMPUTE PerceptionGap = $SYSMIS.
** Sex **.
IF OffenceTypeOffender1Recoded220117 EQ 1 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    JurorSeverityComparedWithJudgeStage1 EQ -1 AND Survey1_B1_d_Sex GE 4 AND Survey2_B1_d_Sex GE 4 PerceptionGap = 1.
IF OffenceTypeOffender1Recoded220117 EQ 1 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    (JurorSeverityComparedWithJudgeStage1 GT -1 OR Survey1_B1_d_Sex LT 4 OR Survey2_B1_d_Sex LT 4) PerceptionGap = 0.
IF OffenceTypeOffender1Recoded220117 EQ 1 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    (NMISS(JurorSeverityComparedWithJudgeStage1,Survey1_B1_d_Sex,Survey2_B1_d_Sex) GT 0) PerceptionGap = 98.

** Violence **.
IF OffenceTypeOffender1Recoded220117 EQ 2 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    JurorSeverityComparedWithJudgeStage1 EQ -1 AND Survey1_B1_a_Violent GE 4 AND Survey2_B1_a_Violent GE 4 PerceptionGap = 1.
IF OffenceTypeOffender1Recoded220117 EQ 2 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    (JurorSeverityComparedWithJudgeStage1 GT -1 OR Survey1_B1_a_Violent LT 4 OR Survey2_B1_a_Violent LT 4) PerceptionGap = 0.
IF OffenceTypeOffender1Recoded220117 EQ 2 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    NMISS(JurorSeverityComparedWithJudgeStage1,Survey1_B1_a_Violent,Survey2_B1_a_Violent) GT 0 PerceptionGap = 98.

** Drugs **.
IF OffenceTypeOffender1Recoded220117 EQ 3 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    JurorSeverityComparedWithJudgeStage1 EQ -1 AND Survey1_B1_c_Drug GE 4 AND Survey2_B1_c_Drug GE 4 PerceptionGap = 1.
IF OffenceTypeOffender1Recoded220117 EQ 3 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    (JurorSeverityComparedWithJudgeStage1 GT -1 OR Survey1_B1_c_Drug LT 4 OR Survey2_B1_c_Drug LT 4) PerceptionGap = 0.
IF OffenceTypeOffender1Recoded220117 EQ 3 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    NMISS(JurorSeverityComparedWithJudgeStage1,Survey1_B1_c_Drug,Survey2_B1_c_Drug) GT 0 PerceptionGap = 98.

** Property **.
IF OffenceTypeOffender1Recoded220117 EQ 4 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    JurorSeverityComparedWithJudgeStage1 EQ -1 AND Survey1_B1_b_Property GE 4 AND Survey2_B1_b_Property GE 4 PerceptionGap = 1.
IF OffenceTypeOffender1Recoded220117 EQ 4 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    (JurorSeverityComparedWithJudgeStage1 GT -1 OR Survey1_B1_b_Property LT 4 OR Survey2_B1_b_Property LT 4) PerceptionGap = 0.
IF OffenceTypeOffender1Recoded220117 EQ 4 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    NMISS(JurorSeverityComparedWithJudgeStage1,Survey1_B1_b_Property,Survey2_B1_b_Property) PerceptionGap = 98.

** Culpable Driving and 'Other' **.
COUNT Comp1 = Survey1_B1_a_Violent TO Survey1_B1_d_Sex (4 THRU 5).
COUNT Comp2 = Survey2_B1_a_Violent TO Survey2_B1_d_Sex (4 THRU 5).

IF OffenceTypeOffender1Recoded220117 EQ 5 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    JurorSeverityComparedWithJudgeStage1 EQ -1 AND Comp1 GE 3 AND Comp2 GE 3 PerceptionGap = 1.
IF OffenceTypeOffender1Recoded220117 EQ 5 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    (JurorSeverityComparedWithJudgeStage1 GT -1 OR Comp1 LT 3 OR Comp2 LT 3) PerceptionGap = 0.
IF OffenceTypeOffender1Recoded220117 EQ 5 AND JurorCompletedSurvey2YesNo EQ 1 AND 
    NMISS(Survey1_B1_a_Violent TO Survey2_B1_d_Sex) GT 0 PerceptionGap = 98.

IF JurorCompletedSurvey2YesNo EQ 1 AND MISSING(JurorSeverityComparedWithJudgeStage1) PerceptionGap = 98.
IF JurorCompletedSurvey2YesNo EQ 0 PerceptionGap = 97.

VARIABLE LABELS
PerceptionGap 'Gap exists in responses to general sentencing questions and responses to a specific case'.

VALUE LABELS
PerceptionGap
0 'No'
1 'Yes'
97 'Did not complete Survey 2'
98 'Cannot calculate - missing data'.

MISSING VALUES
PerceptionGap (97,98).

FORMATS
PerceptionGap (f8).

EXECUTE.

DELETE VARIABLES Comp1 Comp2.
EXECUTE. 

SET ERRORS ON.
