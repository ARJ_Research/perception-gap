﻿* Encoding: UTF-8.
MISSING VALUES Survey2_A20a_SameFeelingsVictim,Survey2_A20b_TakePerspectiveVictim (98,99).

Compute Survey2_Sympathy = $SYSMIS.
IF JurorCompletedSurvey2YesNo EQ 1 Survey2_Sympathy = 
    (MEAN(Survey2_A16a_ThoughtsDefendantsHead TO Survey2_A16f_EasilyTakePerspective) - MEAN(Survey2_A20a_SameFeelingsVictim,Survey2_A20b_TakePerspectiveVictim)).
IF JurorCompletedSurvey2YesNo EQ 0 Survey2_Sympathy = 97.
EXECUTE.

VARIABLE LABELS
Survey2_Sympathy 'Sympathy - Difference between mean rating of sympathy for defendant and mean rating of sympathy for victim'.

VALUE LABELS
Survey2_Sympathy
97 'Did not complete Survey 2'.

MISSING VALUES
Survey2_Sympathy (97).

FORMATS
Survey2_Sympathy (f8).