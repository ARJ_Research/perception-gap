﻿* Encoding: UTF-8.
** Only calculate score for Survey 2 Jurors **.
COMPUTE Survey2_FAB_Violent = $SYSMIS.
COMPUTE Survey2_FAB_Property = $SYSMIS.
COMPUTE Survey2_FAB_Drug = $SYSMIS.
COMPUTE Survey2_FAB_Sex = $SYSMIS.

** Violent **.
IF JurorCompletedSurvey2YesNo EQ 1
    Survey2_FAB_Violent = Survey2_B2_a_a_IndividOffender_Violent - Survey2_B2_b_a_EnviroSituational_Violent.
IF JurorCompletedSurvey2YesNo EQ 1 AND (MISSING(Survey2_B2_a_a_IndividOffender_Violent) OR 
    MISSING(Survey2_B2_b_a_EnviroSituational_Violent)) Survey2_FAB_Violent = 98.
IF JurorCompletedSurvey2YesNo EQ 0 Survey2_FAB_Violent = 97. 
EXECUTE.

** Property **.
IF JurorCompletedSurvey2YesNo EQ 1
    Survey2_FAB_Property = Survey2_B2_a_b_IndividOffender_Property - Survey2_B2_b_b_EnviroSituational_Property.
IF JurorCompletedSurvey2YesNo EQ 1 AND (MISSING(Survey2_B2_a_b_IndividOffender_Property) OR 
    MISSING(Survey2_B2_b_b_EnviroSituational_Property)) Survey2_FAB_Property = 98.
IF JurorCompletedSurvey2YesNo EQ 0 Survey2_FAB_Property = 97. 
EXECUTE.

** Drug **.
IF JurorCompletedSurvey2YesNo EQ 1
    Survey2_FAB_Drug = Survey2_B2_a_c_IndividOffender_Drug - Survey2_B2_b_c_EnviroSituational_Drug.
IF JurorCompletedSurvey2YesNo EQ 1 AND (MISSING(Survey2_B2_a_c_IndividOffender_Drug) OR 
    MISSING(Survey2_B2_b_c_EnviroSituational_Drug)) Survey2_FAB_Drug = 98.
IF JurorCompletedSurvey2YesNo EQ 0 Survey2_FAB_Drug = 97. 
EXECUTE.

** Sex **.
IF JurorCompletedSurvey2YesNo EQ 1
    Survey2_FAB_Sex = Survey2_B2_a_d_IndividOffender_Sex - Survey2_B2_b_d_EnviroSituational_Sex.
IF JurorCompletedSurvey2YesNo EQ 1 AND (MISSING(Survey2_B2_a_d_IndividOffender_Sex) OR 
    MISSING(Survey2_B2_b_d_EnviroSituational_Sex)) Survey2_FAB_Sex = 98.
IF JurorCompletedSurvey2YesNo EQ 0 Survey2_FAB_Sex = 97. 
EXECUTE.

** Formatting **.
VARIABLE LABELS
Survey2_FAB_Violent 'Violent Fundamental Attribution Bias - Difference between ratings of indivdual and envirosituational factors'
Survey2_FAB_Property 'Property Fundamental Attribution Bias - Difference between ratings of indivdual and envirosituational factors'
Survey2_FAB_Drug 'Drug Fundamental Attribution Bias - Difference between ratings of indivdual and envirosituational factors'
Survey2_FAB_Sex 'Sex Fundamental Attribution Bias - Difference between ratings of indivdual and envirosituational factors'.

VALUE LABELS
Survey2_FAB_Violent Survey2_FAB_Property Survey2_FAB_Drug Survey2_FAB_Sex
97 'Did not complete Survey 2' 
98 'Cannot calculate - missing data'.

MISSING VALUES
Survey2_FAB_Violent Survey2_FAB_Property Survey2_FAB_Drug Survey2_FAB_Sex (97,98).

FORMATS
Survey2_FAB_Violent Survey2_FAB_Property Survey2_FAB_Drug Survey2_FAB_Sex (f8).
EXECUTE.
