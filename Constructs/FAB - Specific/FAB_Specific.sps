﻿* Encoding: UTF-8.
** Only calculate score for Survey 2 Jurors **.
COMPUTE Survey2_FAB_Specific = $SYSMIS.
IF JurorCompletedSurvey2YesNo EQ 1 Survey2_FAB_Specific = (Survey2_A14a_IndividOffenderFactorsYourTrial - Survey2_A14b_EnviroSituationFactorsYourTrial).
IF JurorCompletedSurvey2YesNo EQ 1 AND (MISSING(Survey2_A14a_IndividOffenderFactorsYourTrial) OR 
    MISSING(Survey2_A14b_EnviroSituationFactorsYourTrial)) Survey2_FAB_Specific = 98.
IF JurorCompletedSurvey2YesNo EQ 0 Survey2_FAB_Specific = 97. 
EXECUTE.

VARIABLE LABELS
Survey2_FAB_Specific 'Specific Fundamental Attribution Bias - Difference between ratings of indivdual and envirosituational factors'.

VALUE LABELS
Survey2_FAB_Specific
97 'Did not complete Survey 2'
98 'Cannot calculate - missing data'.

MISSING VALUES
Survey2_FAB_Specific (97,98).

FORMATS
Survey2_FAB_Specific (f8).